package FXTEST;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.HashMap;
import java.util.Map;

/**
 * Hello world!
 *
 */
public class FXRates 
{
     Map<String,Double> rates =new HashMap<String,Double>();
    public static void main( String[] args )
    {
       FXRates a = new FXRates();
       a.readCSV();
       System.out.println (a.getEXRate ("ZAR", "IDR"));
    }
    
    public  void readCSV()
    {
        String csvFile = "rates.csv";
        BufferedReader br = null;
        String line = "";
        String cvsSplitBy = ",";

        try {

            br = new BufferedReader(new FileReader(csvFile));
            br.readLine ();
            while ((line = br.readLine()) != null) {

                String[] temp = line.split(cvsSplitBy);
                String key = temp[0]+"-"+temp[1];
                double value = Double.parseDouble(temp[2]);
                rates.put(key, value);
                System.out.println(line);

            }

    }
        catch(Exception e)
        {
            
        }
}
    public double getEXRate(String ctr1, String ctr2)
    {
        String key = ctr1+"-"+ctr2;
        
        if(rates.containsKey (key))
        return rates.get (key);
        else if(rates.containsKey (ctr1+"-USD") && (rates.containsKey ("USD-"+ctr2)))
            return rates.get (ctr1+"-USD") * rates.get("USD-"+ctr2);
        else
            return 0.0;
        
    }
}
